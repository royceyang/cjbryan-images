#!/bin/bash

#SBATCH --account=pi-cjbryan
#SBATCH --job-name=Darknet     # name of job
#SBATCH --partition=gpu   # assign the job to a specific queue
#SBATCH --gres=gpu:2
#SBATCH --output=runbatch2.log    # join the output and error files

# load desired python version (check availability with 'module avail')
module load python/booth/3.6/3.6.3

# execute python script
srun make
srun bash run.sh
#srun bash make_into_vid.sh
