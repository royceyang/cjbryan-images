#!/bin/bash

#SBATCH --account=pi-cjbryan
#SBATCH --job-name=Darknet     # name of job
#SBATCH --output=runbatch1.log    # join the output and error files
#SBATCH --cpus-per-task=8
#SBATCH --partition=gpu
#SBATCH --ntasks=1

# load desired python version (check availability with 'module avail')
module load python/booth/3.6/3.6.3

# execute python script
srun rm -rf before_img
srun mkdir before_img
srun bash vid2img.sh
srun rm -rf results
srun mkdir results
