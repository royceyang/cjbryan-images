#!/bin/bash -       
#title           :runvideo.sh
#description     :This script detects objects in a video sand saves the labels.
#author		 :Royce Yang
#date            :20190131
#version         :0.1
#usage		 :bash runvideo.sh video_file.mp4 [-s]
#notes           :Use the -s opt to save visual output;Output folder is called video_file_processed_DATE
#==============================================================================

#SBATCH --account=pi-cjbryan
#SBATCH --job-name=runvideo     # name of job
#SBATCH --partition=gpu   # assign the job to a specific queue
#SBATCH --output=runvid.log    # join the output and error files
#SBATCH --gres=gpu:2
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=8

# load desired python version (check availability with 'module avail')
module load python/booth/3.6/3.6.3

flag=0
debug=0
# use -s flag to indicate that you want to save the video with boxes
while getopts 's' opt; do
    case $opt in
        s) flag=1 ;;
    esac
done

while getopts 'd' opt; do
    case $opt in
        d) debug=1 ;;
    esac
done

FILE1=$1
echo $FILE1
if [[ $FILE1 != *.mp4 ]] # convert to mp4 if not already
then
    if [ "$debug" -eq 1 ]; then
        echo Checkpoint 1
    fi 
    rm before.mp4
    srun ffmpeg -i $FILE1 -vcodec copy -acodec copy before.mp4
else
    if [ "$debug" -eq 1 ]; then
        echo Checkpoint 2
    fi
    cp $FILE1 before.mp4
fi
# FILE1=${FILE1%.*}
# FILE1=${FILE1##*/}


# clean up from last run
echo clean up from last run
rm -rf before_img
mkdir before_img
if [ "$debug" -eq 1 ]; then
    echo Checkpoint 3
fi
srun ffmpeg -i before.mp4 -qscale:v 2 before_img/%06d.jpg
if [ "$debug" -eq 1 ]; then
    echo Checkpoint 4
fi
rm -rf results
mkdir results

# compile and run detector
echo compile and run detector
if [ "$debug" -eq 1 ]; then
    echo Checkpoint 4
fi
srun make
srun ./darknet detect cfg/yolov3.cfg yolov3.weights -thresh 0.4
if [ "$debug" -eq 1 ]; then
    echo Checkpoint 5
fi
# clean up, name output folders, etc.
echo clean up, name output folders, etc.
DATE=`date +%Y-%m-%d`
newdirname="${FILE1}_processed_${DATE}"
echo Output folder is $newdirname
if [ "$debug" -eq 1 ]; then
    echo Checkpoint 6
fi
mkdir $newdirname
if [ "$flag" -eq 1 ]; then
    srun ffmpeg -start_number 1 -i results/%06d.jpg -vcodec mpeg4 test.mp4
    newvideoname="${newdirname}.mp4"
    mv test.mp4 $newvideoname
fi
if [ "$debug" -eq 1 ]; then
    echo Checkpoint 7
fi
rm results/*.jpg
mv results labels
mv labels $newdirname
if [ "$flag" -eq 1 ]; then
    mv $newvideoname $newdirname
fi
mv runvid.log $newdirname
