#!/bin/bash

#SBATCH --account=pi-cjbryan
#SBATCH --job-name=Darknet     # name of job
#SBATCH --output=runbatch3.log    # join the output and error files
#SBATCH --cpus-per-task=8

# load desired python version (check availability with 'module avail')
module load python/booth/3.6/3.6.3

# execute python script
srun bash make_into_vid.sh
rm results/*.jpg
