import subprocess
import argparse
import os
import datetime

parser = argparse.ArgumentParser()
parser.add_argument("videoname", help="input name of the video file that you want to process")
parser.add_argument("--savevid", help="save a video file for visual detection confirmation")
args = parser.parse_args()
#parser.videoname
subprocess.check_output(["bash", "cleanup.sh"])
#subprocess.check_output(["rm", "*.jpg"])
#subprocess.check_output(["cd", ".."])
#subprocess.check_output(["ffmpeg", "-i", args.videoname, "-qscale:v", "2", "before_img/before_%05d.jpg"])
subprocess.check_output(["cp", args.videoname, "before.mp4"])
subprocess.check_output(["bash", "vid2img.sh"])
subprocess.check_output(["bash", "run.sh"])
if (args.savevid):
    subprocess.check_output(["bash", "make_into_vid.sh"])
subprocess.check_output(["bash", "clear_results_img.sh"])
timestr = datetime.datetime.now().strftime("%Y-%m-%d-%H:%M")
subprocess.check_output(["mv", "results", "%s_results_%s" % (os.path.splitext(args.videoname)[0], timestr)])
if (args.savevid):
    subprocess.check_output(["mv", "test.mp4", "%s_results_%s/%s_processed.mp4" % (args.videoname, timestr, os.path.splitext(args.videoname)[0])])
